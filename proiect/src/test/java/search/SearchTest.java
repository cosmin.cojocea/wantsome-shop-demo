package search;

import org.junit.Assert;
import org.junit.Test;
import page_Objects.HomeShopPage;
import page_Objects.SearchResultsPage;
import utils.BaseTestClass;

public class SearchTest extends BaseTestClass {

    @Test

    public void checkSearchResult(){
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnSearchIcon();
        SearchResultsPage searchResults = homeShopPage.typeOnSearch("watch");
        Assert.assertTrue(searchResults.checkResultsContain("watch"));
    }
}
