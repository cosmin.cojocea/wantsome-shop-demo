package utils;

import java.util.List;

public class TotalSumInsideAList {

    //method that calculate the total sum inside a list (with doubles)
    public static double Sum(List<Double> list){
        double sum = 0;
        for (double i: list) {
            sum +=i;
        }
        return  sum;
    }
}
