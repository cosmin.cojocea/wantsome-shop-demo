package apitests;

import org.junit.Test;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class ApiTestsDemo {

    @Test

    public void firstApiTest(){
        get("https://petstore.swagger.io/v2/pet/9222968140496921253")
                .then()
                .assertThat()
                .statusCode(200);
    }


    @Test

    public void getPropertyFromObject(){
        get("https://petstore.swagger.io/v2/pet/9222968140496921253")
                .then()
                .assertThat()
                .body("category.name", equalTo("Doggy"));
    }

    @Test
    public void getPropertyFromArray(){
        get("https://petstore.swagger.io/v2/pet/9222968140496921253")
                .then()
                .assertThat()
                .body("tags.name", hasItem("Labrador"));

    }

    @Test
    public void getCategory(){
        get("https://petstore.swagger.io/v2/pet/9222968140496921253")
                .then()
                .assertThat()
                .body("category.id", equalTo(33))
                .body("category.name", equalTo("Doggy"));
    }

    @Test
    public void status404Test(){
        get("https://petstore.swagger.io/v2/pet/9222968140496921w53")
                .then()
                .assertThat()
                .statusCode(404);
    }

    @Test

    public void errorMsg(){
        get("https://petstore.swagger.io/v2/pet/9222968140496921w53")
                .then()
                .assertThat()
                .body("message", equalTo("la ce ne asteptam"));
    }
}
