package cart;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import page_Objects.*;
import utils.BaseTestClass;
import utils.TotalSumInsideAList;
import utils.UserCredentials;

import java.sql.SQLException;
import java.text.ParseException;


public class CartTests extends BaseTestClass {

    //method used here:log in with a existing user before each test executed inside this class;
    @Before
    public void beforeEachTest() throws SQLException {
        UserCredentials userCredentials = new UserCredentials();
        String username = userCredentials.extractUsernameAndPasswordFromDataBase().get("username");
        String  password= userCredentials.extractUsernameAndPasswordFromDataBase().get("password");
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnConnectLink();
        ConnectPage connectPage= new ConnectPage(driver);
        connectPage.login(username, password);
    }


    //method used here:logs out existing user after each test executed inside this class;
    @After
    public void afterEachTest(){
        MyAccountPage myAccountPage = new MyAccountPage(driver);
        myAccountPage.LogOut();
    }

    //in class
    @Test
    public void checkEmptyCart(){
        //click on Cart tab
        driver.findElement(By.xpath("//a[text()='Cart']")).click();

        //check Cart page title
        driver.findElement(By.className("entry-title"));

        Assert.assertEquals("CART", driver.findElement(By.className("entry-title")).getText());

        //check Cart page message
        Assert.assertEquals("Your cart is currently empty.", driver.findElement(By.className("cart-empty")).getText());


    }

    //in class
    @Test
    public void checkEmptyCartWithPageObject(){
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        CartPage cartPage = homeShopPage.clickOnCart();
        Assert.assertEquals("CART", cartPage.getCartTitle());
        Assert.assertEquals("Your cart is currently empty.", cartPage.getCartMessage());
    }

    @Test
    //in class
    public void checkCartValue() throws InterruptedException {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        ManCategoryPage manCategoryPage = homeShopPage.clickManCollection();
        manCategoryPage.addToCart();
        CartPage cartPage = new CartPage(driver);
        cartPage.getCartValue();
        Thread.sleep(5000);
        Assert.assertEquals(10, cartPage.getCartValue());

    }

    @Test
    //project
    public void checkCartSubtotal() throws InterruptedException, ParseException {
        double DELTA = 1e-15;
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();

        ManCategoryPage manCategoryPage = homeShopPage.clickManCollection();
        manCategoryPage.sortPriceOnMenCollectionPageHighToLow();
        manCategoryPage.addToCart();
        manCategoryPage.sortPriceOnMenCollectionPageLowToHigh();
        manCategoryPage.addToCart();
        manCategoryPage.viewCartAction();

        CartPage cartPage = new CartPage(driver);
        cartPage.clickOnQuantity();
        Thread.sleep(2000);
        double sumOfAllPricesFromCartSubtotal = TotalSumInsideAList.Sum(cartPage.getFinalPriceListAddedInCart());
        double cartSubtotal = cartPage.getCartTotalsSubtotal();
        Assert.assertEquals(sumOfAllPricesFromCartSubtotal, cartSubtotal, DELTA);
    }

    @Test
    public void testPriceMultiplyWithQuantityEqualSubtotalOnPageCart() throws InterruptedException {
        double DELTA = 1e-15;
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();

        ManCategoryPage manCategoryPage = homeShopPage.clickManCollection();
        manCategoryPage.sortPriceOnMenCollectionPageHighToLow();
        manCategoryPage.addToCart();
        manCategoryPage.sortPriceOnMenCollectionPageLowToHigh();
        manCategoryPage.addToCart();
        manCategoryPage.viewCartAction();

        CartPage cartPage = new CartPage(driver);
        cartPage.clickOnQuantity();

        double quantityAfferentToFirstProduct = cartPage.getQuantityListFromCartPage().get(0);
        double firstProductPriceFromList = cartPage.getPriceListAddedInCart().get(0);
        double firstSubtotalPriceOfTheFirstProduct = cartPage.getFinalPriceListAddedInCart().get(0);
        double quantityAfferentToLastProduct = cartPage.getQuantityListFromCartPage().get(cartPage.getQuantityListFromCartPage().size()-1);
        double lastProductPriceFromList = cartPage.getPriceListAddedInCart().get(cartPage.getPriceListAddedInCart().size()-1);
        double lastSubtotalPriceOfTheFirstProduct = cartPage.getFinalPriceListAddedInCart().get(cartPage.getFinalPriceListAddedInCart().size()-1);

        System.out.println("cantitatea aferenta primului produs este " + quantityAfferentToFirstProduct);
        System.out.println("pretul primului produs este " + firstSubtotalPriceOfTheFirstProduct);
        System.out.println("pretul subtotal primului produs este " + firstSubtotalPriceOfTheFirstProduct);
        System.out.println("cantitatea aferenta ultimului produs este " + quantityAfferentToLastProduct);
        System.out.println("pretul ultimului produs este "+ lastProductPriceFromList);
        System.out.println("pretul subtotal ultimului produs este " + lastSubtotalPriceOfTheFirstProduct);

        Assert.assertEquals(quantityAfferentToFirstProduct  *   firstProductPriceFromList, firstSubtotalPriceOfTheFirstProduct,DELTA);
        Assert.assertEquals(quantityAfferentToLastProduct * lastProductPriceFromList, lastSubtotalPriceOfTheFirstProduct,DELTA);
    }
}
