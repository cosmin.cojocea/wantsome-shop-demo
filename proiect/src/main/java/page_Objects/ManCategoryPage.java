package page_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManCategoryPage {
    WebDriver driver;
    private final By MEN_TITLE = By.className("entry-title");
    private final By CART_ICON = By.className("add_to_cart_button");
    private final By HOVER_IMAGE = By.className("products-hover-wrapper");
    private final By PRODUCT_PRICE = By.cssSelector("ul.products li :not(del) >.amount");
    private final By VIEW_CART = By.className("wc-forward");


    public ManCategoryPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getManCategoryTitle(){
        return driver.findElement(MEN_TITLE).getText();
    }

    //method that process the prices in the page Men Collection and return a list with all prices
    public List<Double> getProductsPrice(){
        List<WebElement> productsPrices = driver.findElements(PRODUCT_PRICE);
        List<Double> priceList = new ArrayList<Double>();
        for(WebElement price: productsPrices){
            String productPrice = price.getText();
            //System.out.println(productPrice);
            String[] priceValue = productPrice.split(" ");
            //System.out.println(priceValue[0]);
            Double priceValues = Double.parseDouble(priceValue[0].replace(",", "."));
            priceList.add(priceValues);
            //System.out.println(priceValues);
        }
            //System.out.println(productsPrices.size());
        return priceList;
    }
    //method that get the maximum value of the price from prices displayed on Men Collection page
    public double getMaximumValueProductsPrice(){
        return Collections.max(getProductsPrice());
    }

    //method that get the minim value of price from prices displayed on Men Collection page
    public double getMinimalValueProductsPrice(){
        return  Collections.min(getProductsPrice());
    }

    //method that is adding first product inside cart from MenCollection page;
    public void addToCart() {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.cssSelector(".products-img a img")))
                .build()
                .perform();
        actions.moveToElement(driver.findElement(By.xpath("//figure//div[@class='products-hover-block']")))
                .build()
                .perform();
        driver.findElement(CART_ICON).click();
    }

    //method that choose from dropdown sorting from page ManCollection the option "low to high";
    public void sortPriceOnMenCollectionPageLowToHigh (){
        WebElement dropdown = driver.findElement(By.className("orderby"));
        Select optionDropdown = new Select(dropdown);
        optionDropdown.selectByValue("price");
    }
    //method that choose from dropdown sorting from page ManCollection the option "high to low";
    public void sortPriceOnMenCollectionPageHighToLow (){
        WebElement dropdown = driver.findElement(By.className("orderby"));
        Select optionDropdown = new Select(dropdown);
        optionDropdown.selectByValue("price-desc");
    }

    public void viewCartAction (){
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.className("wishlist-cart-wrapper")))
                .build()
                .perform();
        driver.findElement(VIEW_CART).click();

    }
}




