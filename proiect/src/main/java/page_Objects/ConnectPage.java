package page_Objects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ConnectPage {
    WebDriver driver;
    public ConnectPage(WebDriver driver) {
        this.driver = driver;
    }
    private final By USERNAME= By.id("username");
    private final By PASSWORD=By.id("password");
    private final By LOGIN_BUTTON=By.className("woocommerce-button");



    public void login(String username, String password){
        driver.findElement(USERNAME).click();
        driver.findElement(USERNAME).sendKeys(username);
        driver.findElement(PASSWORD).click();
        driver.findElement(PASSWORD).sendKeys(password);
        driver.findElement(LOGIN_BUTTON).click();


    }


}
