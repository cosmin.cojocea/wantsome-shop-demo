package page_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchResultsPage {
    WebDriver driver;
    private final By SEARCH_RESULTS = By.cssSelector("[id='primary'] [class='entry-title']");

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
    }
    public boolean checkResultsContain(String searchInput){
        List<WebElement> searchResults = driver.findElements(SEARCH_RESULTS);
        for (WebElement result: searchResults){
            if (!result.getText().toLowerCase().contains(searchInput)){
                return false;
            }
        }
        return true;
    }
}
