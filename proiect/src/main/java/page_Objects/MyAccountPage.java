package page_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyAccountPage {
    WebDriver driver;
    private final By MY_ACCOUNT_ICON = By.className("user-icon");
    private final By LOG_OUT = By.xpath("//div[@class='woocommerce-MyAccount-content']/p/a");

    public MyAccountPage(WebDriver driver){
        this.driver=driver;
    }

    public void LogOut (){
        driver.findElement(MY_ACCOUNT_ICON).click();
        driver.findElement(LOG_OUT).click();
    }
}
