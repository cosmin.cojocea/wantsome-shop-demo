package page_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
public class CartPage {
    WebDriver driver;
    public CartPage(WebDriver driver) {
        this.driver = driver;
    }
    private final By CART_TITLE = By.className("entry-title");
    private final By CART_MESSAGE = By.className("cart-empty");
    private final By CART_VALUE = By.className("cart-value");
    private final By PRICE = By.cssSelector("div.entry-thumbnail table.shop_table td.product-price span.woocommerce-Price-amount");
    private final By QUANTITY = By.cssSelector("div.entry-thumbnail table.shop_table td.product-quantity div.quantity [value]");
    private final By SUBTOTAL_PRICE = By.cssSelector("div.entry-thumbnail table.shop_table tr.woocommerce-cart-form__cart-item td.product-subtotal span.woocommerce-Price-amount");
    private final By UPDATE_CART =By.name("update_cart");
    private final By SUBTOTAL = By.cssSelector("div.entry-thumbnail table.shop_table tr.woocommerce-cart-form__cart-item td.product-subtotal span.woocommerce-Price-amount");// div.entry-thumbnail table.shop_table td.product-subtotal span.woocommerce-Price-amount
    private final By CART_SUBTOTAL = By.xpath("//tr[@class='cart-subtotal']/td[@data-title='Subtotal']/span[contains(@class, 'Price-amount')]");
    private final By CART_UPDATE_MESSAGE = By.className("woocommerce-message");




    public String getCartTitle(){
        return driver.findElement(CART_TITLE).getText();
    }


    public String getCartMessage(){
        return driver.findElement(CART_MESSAGE).getText();
    }


    public double getCartValue(){
        return  Double.parseDouble(driver.findElement(CART_VALUE).getText());
    }

    public double getCartTotalsSubtotal(){
        String priceValueWithLei=driver.findElement(CART_SUBTOTAL).getText();
        String[] priceValueWithoutLei=priceValueWithLei.split(" ");
        System.out.println(priceValueWithoutLei);
        String cartSubtotalPrice=priceValueWithoutLei[0].replace(".", "");
        cartSubtotalPrice = cartSubtotalPrice.replace(",", ".");
        Double finalCartSubtotalPrice=Double.parseDouble(cartSubtotalPrice);
        System.out.println(finalCartSubtotalPrice);
        return finalCartSubtotalPrice;
    }


    //method that add for quantity for each product added in cart random quantity
    public void clickOnQuantity() throws InterruptedException {
        List<WebElement> numberOfQuantity = driver.findElements(QUANTITY);
        for (WebElement eachClick : numberOfQuantity){
            Random random= new Random();
            String randomNo = String.valueOf(random.nextInt(6)+1); //->int intre 1 si 6;
            eachClick.click();
            eachClick.sendKeys(Keys.BACK_SPACE);
            eachClick.sendKeys(randomNo);
        }
        driver.findElement(UPDATE_CART).click();
        WebDriverWait webDriverWait = new WebDriverWait(driver,10);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(CART_UPDATE_MESSAGE));
        System.out.println(driver.findElement(CART_UPDATE_MESSAGE).getText());
    }

    //method that gets the price lists added in cart (without quantity, just prices)
    public List<Double> getPriceListAddedInCart() throws InterruptedException {
        List<WebElement> productsThatAreAddedInCart = driver.findElements((PRICE));
        List<Double> priceList = new ArrayList<>();
        for (WebElement price : productsThatAreAddedInCart) {
            String productPrice = price.getText();
            String[] priceValue = productPrice.split(" ");
            String productPriceValue = priceValue [0];
            if(productPriceValue.contains(".")){
                productPriceValue = priceValue[0].replace(".","");
            }
            Double priceValues = Double.parseDouble((productPriceValue.replace(",", ".")));
            priceList.add(priceValues);
        }
        System.out.println("Lista cu preturi per produs de pe pagina cart este ->>>" + priceList);
        return priceList;

    }

    //method that gets the sum of the price lists added in cart (with quantity added already)
    // public double getSumOfFinalPriceListAddedInCart (){
    //   List<WebElement> subtotalPrices = driver.findElements(SUBTOTAL);
    //    List <Double> subtotalPriceList = new ArrayList<>();
    //    for (WebElement subtotalPrice : subtotalPrices) {
    //         String [] priceValue= subtotalPrice.getText().split(" ");
    //         String productSubtotalPrice = priceValue[0];
    //         if (productSubtotalPrice.contains(".")){
    //             productSubtotalPrice = priceValue[0].replace(".", "");
    //         }
    //         productSubtotalPrice = productSubtotalPrice.replace(",", ".");
    //         Double subtotalPriceValues = Double.parseDouble(productSubtotalPrice);
    //         subtotalPriceList.add(subtotalPriceValues);
    //    }

    //    return (subtotalPriceList);

    //}

    //method that gets the list of prices added in cart (with quantity added already)
    public List<Double> getFinalPriceListAddedInCart(){
        List<WebElement> subtotalPrices = driver.findElements(SUBTOTAL);
        List <Double> subtotalPriceList = new ArrayList<>();
        for (WebElement subtotalPrice : subtotalPrices) {
            String [] priceValue= subtotalPrice.getText().split(" ");
            String productSubtotalPrice = priceValue[0];
            if (productSubtotalPrice.contains(".")){
                productSubtotalPrice = priceValue[0].replace(".", "");
            }
            productSubtotalPrice = productSubtotalPrice.replace(",", ".");
            Double subtotalPriceValues = Double.parseDouble(productSubtotalPrice);
            subtotalPriceList.add(subtotalPriceValues);
        }
        System.out.println("Lista preturi finale de pe pagina cart este ->>> " + subtotalPriceList);
        return  subtotalPriceList;

    }


    public List<Double> getQuantityListFromCartPage(){

        List<WebElement> quantity = driver.findElements(QUANTITY);
        List <Double>  quantityList= new ArrayList<>();
        for (WebElement priceQuantity : quantity){
            Double quantityToPrice= Double.parseDouble(priceQuantity.getAttribute("Value"));
            quantityList.add(quantityToPrice);
        }
        System.out.println("Lista cantitati de pe pagina cart este ->>>" + quantityList);
        return quantityList;

    }






}



