package page_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class HomeShopPage {
    WebDriver driver;

    public HomeShopPage(WebDriver driver) {
        this.driver = driver;
    }
    private final By CART_BUTTON = By.xpath("//a[text()='Cart']");
    private final By CATEGORY_BUTTON = By.className("category-toggle");
    private final By MAN_COLLECTION_BUTTON = By.id("menu-item-509");
    private final By SEARCH_BUTTON = By.className("search-icon");
    private final By SEARCH_FIELD = By.className("search-field");
    private final By WOMEN_COLLECTION_BUTTON = By.id("menu-item-510");
    private final By CONNECT_LINK=By.className("nmr-logged-out");


    public ConnectPage clickOnConnectLink(){
        driver.findElement(CONNECT_LINK).click();
        return new ConnectPage(driver);
    }

    public CartPage clickOnCart(){
        driver.findElement(CART_BUTTON).click();
        return new CartPage(driver);
    }

    public void clickOnCategory(){
        driver.findElement(CATEGORY_BUTTON).click();
    }

    public ManCategoryPage clickManCollection(){
        driver.findElement(MAN_COLLECTION_BUTTON).click();
        return new ManCategoryPage(driver);

    }

    public ManCategoryPage clickWomenCollection(){
        driver.findElement(WOMEN_COLLECTION_BUTTON).click();
        return new ManCategoryPage(driver);
    }

    public void clickOnSearchIcon(){
        driver.findElement(SEARCH_BUTTON).click();
    }

    public SearchResultsPage typeOnSearch(String searchInput){
        driver.findElement(SEARCH_FIELD).sendKeys(searchInput);
        driver.findElement(SEARCH_FIELD).sendKeys(Keys.ENTER);
        return new SearchResultsPage(driver);
    }
}
